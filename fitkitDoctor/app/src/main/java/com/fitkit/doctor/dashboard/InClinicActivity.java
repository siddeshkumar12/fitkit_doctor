package com.fitkit.doctor.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.fitkit.doctor.R;
import com.fitkit.doctor.auth.LoginActivity;
import com.fitkit.doctor.auth.RegistrationActivity;
import com.fitkit.doctor.dashboard.adapter.InClinicAppointmentAdapter;
import com.fitkit.doctor.databinding.ActivityInClinicBinding;
import com.fitkit.doctor.profile.PatientDetailsActivity;

import java.util.ArrayList;

public class InClinicActivity extends AppCompatActivity implements OnclickListener{

    private ActivityInClinicBinding mBinding;
    private ArrayList<String> mArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_clinic);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_in_clinic);

        mBinding.screenTitleTxt.setText("InClinic Consultations");

        mBinding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
//        MyListAdapter adapter = new MyListAdapter(myListData);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        recyclerView.setAdapter(adapter);
        mArrayList.add("Dr. Geeta");
        mArrayList.add("Dr. Sushma");
        mArrayList.add("Dr. Raghu");
        mArrayList.add("Dr. Ramesh");
        InClinicAppointmentAdapter adapter = new InClinicAppointmentAdapter(this,mArrayList,this);
        mBinding.inclinicAppointmentsRv.setHasFixedSize(true);
        mBinding.inclinicAppointmentsRv.setLayoutManager(new LinearLayoutManager(this));
        mBinding.inclinicAppointmentsRv.setAdapter(adapter);
    }

    @Override
    public void OnAppointmentClick() {

        Intent intent = new Intent(InClinicActivity.this, PatientDetailsActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_from_right,R.anim.slide_out_from_left);

    }
}