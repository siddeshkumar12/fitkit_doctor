package com.fitkit.doctor.profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.fitkit.doctor.R;
import com.fitkit.doctor.databinding.ActivityPatientDetailsBinding;

public class PatientDetailsActivity extends AppCompatActivity {

    private ActivityPatientDetailsBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_patient_details);

        mBinding.screenTitleTxt.setText("Patient Details");

        mBinding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}